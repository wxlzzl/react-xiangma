import myAxios from "../request/http";

const Common = {
    postUpload:'/cos/upload'
}

export function postUpload(){
    return myAxios({
        url:Common.postUpload,
        method:'post',
        isToken:true,
        data
    })
}
